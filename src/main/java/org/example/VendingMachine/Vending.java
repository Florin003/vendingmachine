package org.example.VendingMachine;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class Vending {

	static Scanner Scanner = new Scanner(System.in);

	public static void main(String[] args) {
		ExecutieMetode();
	}

	public static int ExecutieMetode() {
		double Food = SelecteazaProdusulDorit();
		double Price = ReturneazaPretulInFunctieDeProdusulAles(Food);
		double Change = ReturneazaPretulProdusului(Price);
		double Boss = ExecutaCalculeleSiOferaRestulDeLaProdus(Price);
		double refresh = AlegeDacaVreiSauNuIncaUnProdus();
		return 0;
	}

	public static int SelecteazaProdusulDorit() {
		int choise = 0;
		try {
			System.out.println("BUNA ZIUA !!");
			System.out.println("Te rog sa introduci numarul produsului dorit:");

			List<String> produse = new ArrayList<String>(10);
			produse.add("1.Snickers ----> 2.5 Lei");
			produse.add("2.Milka ----> 5 Lei");
			produse.add("3.Cola ----> 3 Lei");
			produse.add("4.Pepsi ----> 3 Lei");
			produse.add("5.Fanta ----> 3 Lei");
			produse.add("6.M&M's ----> 6 Lei");
			produse.add("7.Guma Orbit ----> 2.5 Lei");
			produse.add("8.Mars ----> 7.5 Lei");
			produse.add("9.Tedi ----> 1.5 Lei");
			produse.add("10.Skittles ----> 6.5 Lei");

			for (String element : produse) {
				System.out.println(element);
			}

			choise = Scanner.nextInt();

		} catch (java.util.InputMismatchException e) {
			System.out.println("Te rog sa introduci o cifra !");
			return SelecteazaProdusulDorit();
		}
		return choise;
	}

	public static double ReturneazaPretulInFunctieDeProdusulAles(double aleg) {
		if (aleg <= 10) {
			if (aleg == 1)
				return 2.5;
			if (aleg == 2)
				return 5;
			if (aleg == 3)
				return 3;
			if (aleg == 4)
				return 3;
			if (aleg == 5)
				return 3;
			if (aleg == 6)
				return 6;
			if (aleg == 7)
				return 2.5;
			if (aleg == 8)
				return 7.5;
			if (aleg == 9)
				return 1.5;
			if (aleg == 10)
				return 6.5;

		} else {
			System.out.println("NU AI VOIE !!!");
		}

		return aleg;

	}

	public static double ReturneazaPretulProdusului(double Price) {
		// double money = 0;
		System.out.println("Produsul ales costa " + Price + "Lei");

		return Price;

	}

	public static double ExecutaCalculeleSiOferaRestulDeLaProdus(double Price) {
		System.out.println("Accept doar: 10Lei, 5Lei, 1Leu, 0.50Bani.\n INTRODUCETI BANII:");
		double bani = Scanner.nextDouble();

		if (Price <= bani) {
			if (bani <= 10) {
				System.out.println("Foarte bine.");

				double change = bani - Price;
				int stock = 10;
				System.out.println("Ai ramas cu: " + change + "Lei ");
				double zece = (int) change / 10;
				change = change % 10;
				double cinci = (int) change / 5;
				change = change % 5;
				double unu = (int) change / 1;
				change = change % 1;
				double bani50 = (int) (change / 0.5);
				change = change % 0.5;

				System.out.println(zece + " de 10 de lei");
				System.out.println(cinci + " de 5 de lei");
				System.out.println(unu + " de 1 de leu");
				System.out.println(bani50 + " de 50 de bani");
				System.out.println("Stock ramas:" + (stock - 1) + "produse");

			} else {
				System.out.println("Nu se accepta bacnote mai mari de 10 lei !!");
			}
		} else {
			System.out.println("Nu sunt suficienti bani !!");
		}
		return bani;
	}

	public static int AlegeDacaVreiSauNuIncaUnProdus() {
		int y = 1;
		System.out.println("Vrei sa mai cumperi ceva? YES = 1 & NO = 0");
		int alege = Scanner.nextInt();
		if (alege == y) {
			return ExecutieMetode();
		} else {
			System.out.println("La revedere !!");
		}
		return 0;

	}

}
